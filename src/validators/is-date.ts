import {ValidationFunction} from '../interfaces';
import {BAD_TYPE} from '../error-keys';

export function isDate<TProps>(): ValidationFunction<TProps>
{
    return value => value instanceof Date ? null : [{
        errorKey: BAD_TYPE,
        details: {
            expected: 'date',
            value: value
        }
    }];
}