import {ValidationFunction} from '../interfaces';
import {isTypeOf} from './is-type-of';

export function isString<TProps>(): ValidationFunction<TProps>
{
    return isTypeOf('string');
}