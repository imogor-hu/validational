import {ValidationFunction} from '../interfaces';
import {BAD_TYPE} from '../error-keys';

export function isIterable<TProps>(): ValidationFunction<TProps>
{
    return value => value != null && typeof value[Symbol.iterator] === 'function' ? null : [{
        errorKey: BAD_TYPE,
        details: {
            expected: 'iterable',
            value
        }
    }];
}