import {ValidationFunction} from '../interfaces';
import {BAD_TYPE} from '../error-keys';

export function isNull<TProps>(): ValidationFunction<TProps>
{
    return value => null === value ? null : [{
        errorKey: BAD_TYPE,
        details: {
            expected: 'null',
            got: typeof value,
            value
        }
    }];
}