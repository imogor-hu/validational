import {ValidationFunction} from '../interfaces';
import {OUT_OF_RANGE} from '../error-keys';

export function isBetween<TProps, T>(min: T, max: T): ValidationFunction<TProps>
{
    return value => min <= value && value <= max ? null : [{
        errorKey: OUT_OF_RANGE,
        expected: { min, max },
        value
    }];
}