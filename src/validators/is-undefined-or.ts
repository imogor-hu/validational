import {anyOf} from './any-of';
import {ValidationFunction} from '../interfaces';
import {isUndefined} from './is-undefined';

export function isUndefinedOr<TProps>(validator: ValidationFunction<TProps>): ValidationFunction<TProps>
{
    return anyOf([
        isUndefined(),
        validator
    ]);
}