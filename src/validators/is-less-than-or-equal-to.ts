import {ValidationFunction} from '../interfaces';
import {OUT_OF_RANGE} from '../error-keys';

export function isLessThanOrEqualTo<TProps, T>(max: T): ValidationFunction<TProps>
{
    return value => value <= max ? null : [{
        errorKey: OUT_OF_RANGE,
        expected: { max },
        value
    }];
}