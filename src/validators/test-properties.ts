import {NoEarlyExitProps, ValidationError, ValidationFunction, ValidationResult} from '../interfaces';
import {isObject} from './is-object';

export function testProperties<TProps extends NoEarlyExitProps>(predicate: (key: string, value: any) => ValidationResult): ValidationFunction<TProps>
{
    const preconditionValidator = isObject();

    return async (value, props) => {
        const preconditionErrors = preconditionValidator(value, props);
        if (preconditionErrors)
            return preconditionErrors;

        const noEarlyExit = props.noEarlyExit;

        const errors: ValidationError[] = [];
        
        let index = 0;
        for (const property of Object.entries(value))
        {
            const currentErrors =  await predicate(property[0], property[1]);

            if (currentErrors)
            {
                for (const error of currentErrors)
                {
                    (error.path || (error.path = [])).unshift(index);
                }

                errors.push(...currentErrors);

                if (!noEarlyExit)
                    break;
            }
            
            index++;
        }
        
        return errors.length ? errors : null;
    };
}